import Vue from 'vue'
import Vuex from 'vuex'
import User from './modules/user/index' // Module [User] token - User data
import Content from './modules/content/index' // Module [Content]
import VuexPersist from 'vuex-persist' // Vuex Persist save module User in localstorage
import Country from './modules/country/index'
import Categories from './modules/categories/index'
import Firebase from './modules/firebase' // Firebase token handler
import Notifications from './modules/notifications' // Save notifications
import DarkMode from './modules/dark_mode'
import Subscriptions from './modules/subscriptions'

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: 'auth',
  storage: localStorage,
  modules: ["User"]
});

export default new Vuex.Store({

  modules: {
    User,
    Content,
    Country,
    Categories,
    Firebase,
    Notifications,
    DarkMode,
    Subscriptions
  },
  plugins: [vuexPersist.plugin]
})
