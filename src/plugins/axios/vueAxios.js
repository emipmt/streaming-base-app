import Vue from "vue";
import Axios from "axios";
import VueAxios from "vue-axios";

// Axios.defaults.baseURL = "http://165.227.55.182:3000/api"; // API url
// Axios.defaults.baseURL = "http://localhost:3000/api"; // API url dev
Axios.defaults.baseURL = "https://vidaflix.com/api" // API con https
Vue.use(VueAxios, Axios);