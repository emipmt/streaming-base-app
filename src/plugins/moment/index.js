import moment from 'moment'

moment.locale("es", {
    months: [
        "Enero", "Febrero", "Marzo", "Abril",
        "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ],
    monthsShort: [
        "En", "Feb", "Mar", "Ab",
        "May", "Jun", "Jul", "Ag",
        "Sep", "Oct", "Nov", "Dic"
    ]
})

export default moment