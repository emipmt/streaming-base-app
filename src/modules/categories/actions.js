import Vue from 'vue';

export async function getAllCategories({ commit }, token) {
    try {

        const response = await Vue.axios.get("/section", {
            headers: { "Authorization": `Bearer ${token}` }
        });

        const data = response.data.data;
        commit("Categories/setCategories", data, { root: true });

    } catch (e) {
    }
}