export function contents(state) {
    if (state.homeSectionName === "home") {
        return state.contents;
    } else if (state.homeSectionName !== "home" && state.homeSectionId) {
        var contents = state.contents.filter(content => {
            return content.section._id == state.homeSectionId
        });
        if (contents.length > 0) {
            return contents;
        } else {
            return state.contents;
        }
    } else {
        return []
    }
}

export function contentDestacate(state){
    // return state.contents
    // console.log(state.contents)
    if(state.homeSectionName === "home"){
        return state.contents.filter(content => {
            return content.active == true;
        });
    }
    else {
        return state.contents.filter(content => {
            return content.section._id == state.homeSectionId
        })
    }
}