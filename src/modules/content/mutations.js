export function setContent(state, content) {
    state.contents = content;
}

export function setHomeSection(state, homeSection) {
    state.homeSectionName = homeSection.sectionName;
    state.homeSectionId = homeSection.sectionId;
}