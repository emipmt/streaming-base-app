import Vue from 'vue';

export async function getAllContents({ commit }, token) {
    Vue.axios.get("content/true", { // Pide solo contenidos activos
        headers: {
            "Authorization": `Bearer ${token}`
        }
    }).then(response => {
        commit("Content/setContent", response.data.data, {
            root: true
        });
    })
    .catch(e => {
        // console.log(e)
    })

}

export async function getContentById({ commit }, data) {
    try {
        const {
            token,
            id
        } = data;

        const response = await Vue.axios.get(`content/_id/${id}`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });

        return response.data.data[0];

    } catch (e) {
        // console.log(e);
    }
}

export async function getContentByCategory({ commit }, data) {

    const { token, id } = data;

    try {
        const response = await Vue.axios.get(`/search/category/${id}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        return response.data.data;

    } catch (e) {
        // console.log(e);
    }
}

export async function getSections({ commit }, data) {
    const { token, id } = data;

    try {
        const response = await Vue.axios.get(`/category/section/${id}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });
        // console.log(response.data);
        return response.data.data;

    } catch (e) {
        // console.log(e);
    }
}

export async function getContentsBySectionAndCategory({ commit }, data) {
    const { token, sectionId, categoryId } = data;

    try {
        const response = await Vue.axios.get(`/content/slide/${sectionId}/${categoryId}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        return response.data.data;

    } catch (e) {
        // console.log(e);
    }
}

export async function addFavorite({ commit }, data) {
    try {
        const { token, user, content } = data;

        const response = await Vue.axios.post("/favorite", { user, content }, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        return response.data.message;

    } catch (e) {
        // console.log(e);
    }
}

export async function getFavorites({ commit }, data) {
    const { token, userId } = data;

    try {
        const response = await Vue.axios.get(`/favorite/${userId}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        return response.data.data;

    } catch (e) {
        // console.log(e);
    }
}

export async function removeFavorite({ commit }, data) {
    try {
        const { id, token } = data;

        const response = await Vue.axios.delete(`/favorite/${id}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        return response.data.message;
    } catch (e) {
        // console.log(e);
    }
}

export async function createRate({ commit }, data) {
    try {
        const { token, userId, contentId, rate } = data;

        const response = await Vue.axios.post("/rate", { userId, contentId, rate }, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        // console.log(data);
        // console.log(response);

    } catch (e) {
        // console.log(e);
    }
}

export async function getRate({ commit }, data) {
    try {
        const { id, token } = data;

        const response = await Vue.axios.get(`/rate/${id}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        // console.log(response.data)

        return response.data.data;

    } catch (e) {
        // console.log(e);
    }
}