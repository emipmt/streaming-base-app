import { SET_TOKEN } from './constants'

export const actions = {
    _setToken({commit}, token){
        commit(SET_TOKEN, token)
    }
}