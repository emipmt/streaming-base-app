import { SET_TOKEN } from './constants'

export const mutations = {
    [SET_TOKEN](state, token){
        state.firebaseToken = token
    }
}