import { mutations } from './mutations'
import { actions } from './actions'

const state = {
    firebaseToken: ""
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}