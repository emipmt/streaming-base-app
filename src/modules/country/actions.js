import Vue from 'vue';

export async function getAllCountries({ commit }) {
    try {
        const response = await Vue.axios.get("country");
        // // console.log(response.data.data);
        commit("Country/setCountries", response.data.data, { root: true });

    } catch (e) {
        // console.log(e)
    }
}

export async function getContentById({ commit }, data) {
    try {
        const {token, id} = data;

        const response = await Vue.axios.get(`content/_id/${id}`, {
            headers: { "Authorization": `Bearer ${token}` }
        });

        return response.data.data[0];

    } catch (e) {
        // console.log(e);
    }
}