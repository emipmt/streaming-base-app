import { actions } from './actions'
import { mutations } from './mutations'

let darkMode = JSON.parse(localStorage.getItem("darkMode"))
darkMode = darkMode == null ? true : darkMode
let theme = "white"
let buttonTheme = "primary"
let labelTheme = "dark"
let navigationBar = "navigation-bar-light"

// Selecciona por defecto el modo oscuro
if(darkMode || darkMode == null){
    // localStorage.setItem("darkMode", true)
    theme = "dark-mode"
    buttonTheme = "light"
    labelTheme = "light",
    navigationBar = "navigation-bar-dark"
}

const state = {
    darkMode,
    theme,
    buttonTheme,
    labelTheme,
    navigationBar
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}