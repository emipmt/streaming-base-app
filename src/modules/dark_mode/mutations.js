import { CHANGE_THEME } from './constants'

export const mutations = {
    [CHANGE_THEME](state, darkMode){
        if(darkMode == "oscuro"){
            // console.log("tema oscuro")
            localStorage.setItem("darkMode", true)
            state.darkMode = true
            state.theme = "dark-mode"
            state.buttonTheme = "light"
            state.labelTheme = "light",
            state.navigationBar = "navigation-bar-dark"
        }
        else {
            // console.log("tema claro")
            localStorage.setItem("darkMode", false)
            state.darkMode = false
            state.theme = "white"
            state.buttonTheme = "primary"
            state.labelTheme = "dark",
            state.navigationBar = "navigation-bar-light"
        }
    }
}