import { CHANGE_THEME } from './constants'

export const actions = {
    _changeTheme({commit}, darkMode){ // true / false
        commit(CHANGE_THEME, darkMode)
    }
}