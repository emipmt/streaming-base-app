import { SAVE_NOTIFICATIONS } from './constants'

export const actions = {
    _saveNotifications({commit}, notifications){
        commit(SAVE_NOTIFICATIONS, notifications)
    }
}