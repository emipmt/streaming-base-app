import { SAVE_NOTIFICATIONS } from './constants'

export const mutations = {
    [SAVE_NOTIFICATIONS](state, notifications){
        state.olderNotifications = notifications.olderNotifications
        state.newNotifications = notifications.newNotifications
    }
}