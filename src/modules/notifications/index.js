import { mutations } from './mutations'
import { actions } from './actions'

const state = {
    olderNotifications: [],
    newNotifications: []
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}