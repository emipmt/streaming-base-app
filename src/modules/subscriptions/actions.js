import { GET_SUBSCRIPTIONS, SELECT_SUBSCRIPTION } from './constants'
import Vue from 'vue'

export const actions = {
    async _getSubscriptions({commit}, userToken){
        /* // console.log("token: ", userToken) */
        const { data } = await Vue.axios({
            method: "GET",
            url: "/subscription",
            headers: {
                "Authorization": `bearer ${userToken}`
            }
        })

        commit(GET_SUBSCRIPTIONS, data.data)
    },
    async _selectSubscription({commit}, subscription){
        commit(SELECT_SUBSCRIPTION, subscription)
    }
}