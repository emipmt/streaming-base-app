import { GET_SUBSCRIPTIONS, SELECT_SUBSCRIPTION } from './constants'

export const mutations = {
    [GET_SUBSCRIPTIONS](state, subscriptions){
        state.subscriptions = subscriptions
    },
    [SELECT_SUBSCRIPTION](state, subscription){
        state.selectedSubscription = subscription
    }
}