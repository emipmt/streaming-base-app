import { actions } from './actions'
import { mutations } from './mutations'

const state = {
    subscriptions: [],
    selectedSubscription: ""
}

export default {
    namespaced: true,
    actions,
    mutations,
    state
}