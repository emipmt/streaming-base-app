export function setToken(state, token) {
    state.user_token = token;
}

export function setUserData(state, user) {
    state.user = user;
}

export function close(state) {
    state.user = null;
    state.user_token = null;
}

export function setPaymentOptions(state, paymentOption){
    state.paymentOptions  = paymentOption
}