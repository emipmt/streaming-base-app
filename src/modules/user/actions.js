import Vue from "vue";
import router from "../../router";
import moment from 'moment'

export async function getUserProfile({commit}, {id, token}){
    try {
        const { data } = await Vue.axios({
            method: "GET",
            url: `/user/${id}`,
            headers: {
                authorization: `bearer ${token}`
            }
        })
        
        commit("User/setUserData", data.user, {root: true})
    } catch (error) {
        // console.log(error)
    }
}

export async function signUp({
    commit
}, user) {
    try {
        const response = await Vue.axios.post("user", user);

        if (!response.data.success) {
            return response.data;
        }

        const token = response.data.data.token;
        const userCreated = response.data.data.user;

        localStorage.setItem("authorization", token)
        // localStorage.setItem

        commit("User/setToken", token, {
            root: true
        });
        commit("User/setUserData", userCreated, {
            root: true
        });

        router.push("choose-subscription")

        return false;

    } catch (e) {
        // console.log(e);
    }

    /* Vue.axios.post("user", user)
     .then(resp => {

         if (resp.data.success !== false) {
             const token = resp.data.data.token;
             const userCreated = resp.data.data.user;

             commit("User/setToken", token, { root: true });
             commit("User/setUserData", userCreated, { root: true });
             router.push("registered");
         }

         // console.log(resp.data);

     })
     .catch(error => {
         console.error(error);
     });*/
}

export async function intentlogin({
    commit
}, credentials) {
    try {
        const response = await Vue.axios.post("user/login", credentials);
        let paymentDetails = {}
        
        if (!response.data.success) {
            return response.data;
        }
        else {
            paymentDetails = await Vue.axios({
                method: "GET",
                url: "customer",
                headers: {
                    authorization: `bearer ${response.data.data.token}`
                }
            })

            // console.log(paymentDetails)
        }

        const token = response.data.data.token;
        const userLogged = response.data.data.user;

        localStorage.setItem("authorization", token)

        commit("User/setToken", token, {
            root: true
        });
        commit("User/setUserData", userLogged, {
            root: true
        });
        commit("User/setPaymentOptions", paymentDetails.data.customer, {
            root: true
        })
        
        if(userLogged.type == 1){
            router.push("/home/slides")
        }
        else if(paymentDetails.data.customer == null && moment(response.data.data.user.expiresDate).isBefore()){
            // router.push("create-payment-option")
            router.push("choose-subscription")
        }
        else if(paymentDetails.data.customer != null && moment(response.data.data.user.expiresDate).isBefore()){
            router.push("purchase-subscription")
        }
        else {
            router.push("registered");
        }

        return false;

    } catch (e) {
        // console.log(e);
    }

    /*Vue.axios.post("user/login", credentials)
        .then(resp => {
            // console.log(resp);
            if (resp.data.success !== false) {
                const token = resp.data.data.token;
                const userLogged = resp.data.data.user;

                commit("User/setToken", token, { root: true });
                commit("User/setUserData", userLogged, { root: true });
                router.push("registered");
            }

        })
        .catch(error => // console.log(error));*/
}

export async function setUserData({commit}, user){
    commit("User/setUserData", user, {
        root: true
    });
}

export async function setPaymentOption({commit}){
    commit("User/setPaymentOptions", true, {
        root: true
    })
}

export function SignOut({
    commit
}) {
    commit("User/close", null, {
        root: true
    });

    localStorage.removeItem("authorization")
    localStorage.removeItem("auth")
    
    router.push("/");
}

export async function updateProfile({
    commit
}, data) {
    try {
        const user = data[0];
        const token = data[1];
        const id = data[2];
        user.id = id;

        const response = await Vue.axios.put(`user/${id}`, user, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });


        if (response.data.success) {
            commit("User/setUserData", user, {
                root: true
            });

            return true
        }

        return false;

    } catch (e) {
        // console.log(e)
    }
}