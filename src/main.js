import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/plugins' // Plugins [Vue Axios]
import rate from 'vue-rate';
import vueVimeoPlayer from 'vue-vimeo-player'
import BootstrapVue from 'bootstrap-vue'

Vue.use(vueVimeoPlayer);
// ionic
import Ionic from '@ionic/vue';
import '@ionic/core/css/ionic.bundle.css';
Vue.use(Ionic);
Vue.use(rate);
Vue.use(BootstrapVue)
//slider

import VueTinySlider from "vue-tiny-slider";
import "tiny-slider/src/tiny-slider.scss";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.component("tiny-slider", VueTinySlider);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')