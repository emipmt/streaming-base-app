import moment from 'moment'
import Vue from 'vue';
import {
  IonicVueRouter
} from '@ionic/vue';
import { VueEasyJwt } from 'vue-easy-jwt'
const jwt = new VueEasyJwt()

Vue.use(IonicVueRouter);

import AvailableSubscriptions from './views/home/profile/AvailableSubscriptions.vue'

const router = new IonicVueRouter({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'main',
      component: () => import('./views/main/Main.vue')
    },

    {
      path: '/register',
      name: 'register',
      component: () => import('./views/auth/Register.vue')
    },

    {
      path: '/login',
      name: 'login',
      component: () => import('./views/auth/Login.vue')
    },

    {
      path: "/privacy-policy",
      name: "privacy-policy",
      component: () => import("./views/privacy/PrivacyPolicy.vue")
    },

    {
      path: '/registered',
      name: 'registered',
      component: () => import('./views/auth/Registered.vue')
    },

    {
      path: '/home',
      name: 'home',
      component: () => import('./views/home/Home.vue'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      },

      children: [
        {
          path: "/home/slides",
          component: () => import('./views/home/slides/Slides.vue'),
          meta: {
            requiresAuth: true,
            validSubscription: true
          }
        },

        {
          path: "/home/profile",
          component: () => import('./views/home/profile/Profile.vue'),
          meta: {
            requiresAuth: true,
            validSubscription: true
          }
        },

        {
          path: "/home/categories",
          component: () => import('./views/home/slides/CategoriesTab'),
          meta: {
            requiresAuth: true,
            validSubscription: true
          }
        },

        {
          path: "/home/favorite",
          component: () =>  import("./views/home/slides/FavoriteTab"),
          meta: {
            requiresAuth: true,
            validSubscription: true
          }
        },

        {
          path: "/home/search",
          component: () =>  import("./views/home/slides/Search"),
          meta: {
            requiresAuth: true,
            validSubscription: true
          }
        },

        {
          path: "/home/notifications",
          component: () =>  import("./views/home/slides/NotificationsTab"),
          meta: {
            requiresAuth: true,
            validSubscription: true
          }
        }
      ],
    },
    {
      path: "/home/subscription",
      component: () => import('./views/home/profile/CurrentSubscription.vue'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },
    {
      path: "/home/subscriptions",
      component: AvailableSubscriptions,
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: "/choose-subscription",
      component: () => import("./views/home/profile/ChooseSubscription.vue"),
      meta: {
        requiresAuth: true,
        validSubscription: false,
        paymentOptionRequired: false
      }
    },

    { // pagar una suscripcion para usuarios que ya tienen metodo de pago pero se expiró su suscripcion
      path: "/purchase-subscription",
      component: () => import("./views/home/profile/PurchaseSubscription.vue"),
      meta: {
        requiresAuth: true,
        validSubscription: false,
        paymentOptionRequired: true
      }
    },

    // Ver opciones de pago
    {
      path: "/home/payment-options",
      component: () => import("./views/home/profile/PaymentOptions.vue"),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    // Crear opcion de pago (Usuario nuevo)
    {
      path: "/create-payment-option",
      component: () => import("./views/home/profile/CreatePaymentOption.vue"),
      meta: {
        requiresAuth: true,
        validSubscription: false
      }
    },
    
    // Añadir opcion de pago (Usuarios que ya cuentan con una opcion de pago)
    {
      path: "/home/add-payment-option",
      component: () => import("./views/home/profile/AddPaymentOption.vue"),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: "/content/:id",
      component: () => import('./views/contents/Content'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: "/beta",
      component: () => import('./views/home/HomeBeta')
    },

    {
      path: '/change-password',
      component: () => import('./views/home/profile/ChangePassword'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: '/change-email',
      component: () => import('./views/home/profile/ChangeEmail'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: '/forget',
      component: () => import('./views/home/profile/ForgotPassword'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: '/update-profile',
      component: () => import('./views/home/profile/UpdateUserProfile'),
      meta: {
        requiresAuth: true,
        validSubscription: true
      }
    },

    {
      path: '/forgot-password',
      component: () => import('./views/auth/ForgotPassword')
    },

    {
      path: "/category/:id",
      component: () =>  import("./views/contents/ContentsByCategory"),
      meta: {
        requiresAuth: false,
        validSubscription: true
      }
    },

    {
      path: "/contents/:sectionId/:categoryId",
      component: () =>  import("./views/contents/ContentAndCategory"),
      meta: {
        requiresAuth: false,
        validSubscription: true
      }
    }
  ]

});

// ObjectId("5d9b7bd0d6f138083957c64d")

router.beforeEach((to, from, next) => {
  to.matched.some(route => {
    const User = JSON.parse(localStorage.getItem("auth"))
    let isAdmin = false
    const token = localStorage.getItem("authorization")
    let isExpired = true
    let paymentOptions = null
    // verifica si el token es valido
    const isTokenExpired = jwt.isExpired(token)

    try {
      if(User.User.user != null){
        isExpired = moment(User.User.user.expiresDate).isBefore()
        if(User.User.user.type == 1){
          isAdmin = true
        }
      }
      paymentOptions = User.User.paymentOptions
    } catch (error) {
      isExpired = true
      paymentOptions = null
    }

    // El administrador puede acceder a cualquier ruta
    if(isAdmin){
      if(!isTokenExpired){
        if(route.path == "/login" || route.path == "/register" || route.path == ""){
          next({ path: "/home/slides" })
        }
        else {
          next()
        }
      }
      else {
        next()
      }
    }
    else if(route.meta.requiresAuth){
      if(isTokenExpired){
        next({path: "/login"})
      }
      else if(!isTokenExpired && !route.meta.validSubscription){
        if(isExpired && paymentOptions == null){
          if(route.path == "/choose-subscription" || route.path == "/create-payment-option"){
            next()
          }
          else {
            next({ path: "/choose-subscription" })
          }
        }
        /* else {
          next()
        } */
        else if(isExpired && paymentOptions != null){
          if(route.path == "/purchase-subscription"){
            next()
          }
          else {
            next({ path: "/purchase-subscription" })
          }
        }
        else {
          next()
        }
        
        /* if(route.meta.paymentOptionRequired){
        }
        else {
        } */
      }
      else {
        /* 
        next() */
        if(route.meta.paymentOptionRequired){
          if(isExpired && paymentOptions == null){
            next({ path: "/choose-subscription" })
          }
          else if(isExpired && paymentOptions != null){
            next({ path: "/purchase-subscription" })
          }
          else {
            next()
          }
        }
        else {
          if(isExpired && paymentOptions == null){
            next({ path: "/choose-subscription" })
          }
          else if(isExpired && paymentOptions != null){
            next({ path: "/purchase-subscription" })
          }
          else {
            next()
          }
          // next()
        }
      }
    }
    else {
      if(!isTokenExpired){
        next({path: "/home/slides"})
      }
      else {
        next()
      }
    }
  })
})

export default router