/* var firebaseConfig = {
    apiKey: "AIzaSyCw5dnpNZLSHiEu_jbDEC_8XW34rYVc53s",
    authDomain: "vida-flix.firebaseapp.com",
    databaseURL: "https://vida-flix.firebaseio.com",
    projectId: "vida-flix",
    storageBucket: "vida-flix.appspot.com",
    messagingSenderId: "990405960834",
    appId: "1:990405960834:web:bcde5768f9d3358aa6809b",
    measurementId: "G-X0S0XQLMTJ"
}; */

// [START initialize_firebase_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '990405960834'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
  // console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});